from os import environ

token = '338705987:AAFVcR1oOKDpoBe5gU_2ZBt3z1rXPFnl06s'

APP_ENV = environ.get('APP_ENV', 'production')
DEBUG = environ.get('DEBUG', 'true') == 'true'
TELEGRAM_BOT_TOKEN = environ.get('TELEGRAM_BOT_TOKEN', token)
LOG_LEVEL = environ.get('LOG_LEVEL', 'DEBUG')
WEBHOOK_HOST = environ.get(
    'WEBHOOK_HOST', 'https://afternoon-lowlands-26457.herokuapp.com/bot')
WEBHOOK_URL = environ.get(
    'WEBHOOK_URL', 'https://afternoon-lowlands-26457.herokuapp.com')
